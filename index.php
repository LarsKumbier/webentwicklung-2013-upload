﻿<?php
require('FirePHPCore/fb.php');

/**
 * Bootstrapping:
 * - existiert upload-verzeichnis?
 * - ist es überhaupt schreibbar?
 */
$targetDir = 'uploads/';

if (!is_dir($targetDir))
	die('Fehler: Upload-Verzeichnis existiert nicht');
if (!is_writable($targetDir))
	die('Fehler: Upload-Verzeichnis ist nicht beschreibbar');


/**
 * Eine Datei wurde hochgeladen
 */
if ($_REQUEST['action'] == 'Upload') {
	// Fehlerbehandlung
	if ($_FILES['userfile']['error'] != UPLOAD_ERR_OK) {
		switch($_FILES['userfile']['error']) {
			case UPLOAD_ERR_INI_SIZE:
				$msg = 'Datei zu groß laut php.ini';
				break;
				
			case UPLOAD_ERR_FORM_SIZE:
				$msg = 'Datei zu groß laut Formular-Angabe';
				break;
			
			case UPLOAD_ERR_PARTIAL:
				$msg = 'Datei wurde nicht vollständig hoch geladen';
				break;
			
			case UPLOAD_ERR_NO_FILE:
				$msg = 'Es wurde keine Datei hochgeladen.';
				break;
				
			case UPLOAD_ERR_NO_TMP_DIR:
				$msg = 'Auf dem Server wurde kein temporäres Verzeichnis definiert.';
				break;
			
			case UPLOAD_ERR_CANT_WRITE:
				$msg = 'Temporäre Datei konnte nicht geschrieben werden.';
				break;
			
			default:
				$msg = 'Some unknown error occured. I\'m very sorry.';
		}
	} else {
		$filename = $_FILES['userfile']['name'];
		if (preg_match('/^\./', $filename)) {
			$msg = 'Datei darf nicht mit einem Punkt beginnen.';
		} else {
			// Sicherheitsprüfung
			$filenamePath = getcwd().'/'.$targetDir.$filename;
			$targetDirPath = realpath(getcwd().'/'.$targetDir);
			if (dirname($filenamePath) != $targetDirPath) {
				die('Hacking attempt');
			} else {
				$finfo = finfo_open(FILEINFO_MIME_TYPE);
				if (!preg_match('/^image\//', $_FILES['userfile']['type']) ||
				    !preg_match('/^image\//', finfo_file($finfo, $_FILES['userfile']['tmp_name']))) {
					$msg = 'Es sind nur Bilder erlaubt.';
				} else {
					$tmpFile  = $_FILES['userfile']['tmp_name'];
					if (!move_uploaded_file($tmpFile, $targetDir.$filename)) {
						$msg = 'Datei konnte nicht ins Upload-Verzeichnis verschoben werden';
					} else {
						$msg = 'Datei '.$filename.' erfolgreich hochgeladen';
					}
				}
			}
		}
	}
}

/**
 * Eine Datei soll gelöscht werden
 */
else if ($_REQUEST['action'] == 'delete') {
	// existiert die Datei überhaupt?
	$filename = $targetDir.$_REQUEST['file'];
	if (!file_exists($filename)) {
		$msg = 'Datei existiert nicht';
	} else {
		$filenamePath = realpath(getcwd().'/'.$filename);
		
		// Sicherheitsprüfung
		$targetDirPath = realpath(getcwd().'/'.$targetDir);
		if (dirname($filenamePath) != $targetDirPath) {
			die('Hacking attempt');
		}
		
		// löschen
		if (!unlink($filenamePath)) {
			$msg = 'Datei '.$filenamePath.' konnte nicht gelöscht werden.';
		} else {
			$msg = 'Datei '.$filename.' erfolgreich gelöscht';
		}
	}
}

?>
<!DOCTYPE html>
<html>
	<head>
		<title>Mein erster Upload</title>
	</head>
	<body>
		<h1>Datei-Upload</h1>
		<?php if (!empty($msg)) : ?>
		<p class="info"><?=$msg?></p>
		<?php endif; ?>
		
		<h2>Vorhandene Dateien</h2>
		<?php if (!($files = glob($targetDir.'*'))) : ?>
		<p>es wurden keine Dateien gefunden.</p>
		<?php else : ?>
		<ul>
		<?php foreach ($files as $file) : ?>
			<li>
				<a href="<?=$_SERVER['PHP_SELF']?>?action=delete&file=<?=urlencode(substr($file, strlen($targetDir)))?>">
					<img src="images/delete.svg">
				</a>
				<a href="<?=$file?>"><?=substr($file, strlen($targetDir))?></a>
			</li>
		<?php endforeach; ?>
		</ul>
		<?php endif; ?>
		
		
		<h2>Upload</h2>
		<form enctype="multipart/form-data"
		      action="<?= $_SERVER['PHP_SELF'] ?>" 
		      method="POST">
			<input type="file"
			       name="userfile"><br>
			<input type="submit"
			       name="action"
			       value="Upload">
		</form>
	</body>
</html>
